require "rails_helper"

RSpec.describe TasksController do
  login_user
  describe "GET index" do
    it "assigns @tasks" do
      task = FactoryBot.create(:task, user: User.first)
      get :index
      expect(assigns(:my_tasks)).to eq([task])
    end

    it 'assigns @public_tasks' do
      public_task = FactoryBot.create(:task, private_task: false)
      get :index
      expect(assigns(:public_tasks)).to eq([public_task])
    end

    it 'public tasks should be blank' do
      FactoryBot.create(:task)
      get :index
      expect(assigns(:public_tasks)).to eq([])
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
  end
  describe "GET show" do
    it "assigns @task" do
      task = FactoryBot.create(:task, user: User.first)
      get :show,  params: { id: task.id }
      expect(assigns(:task)).to eq(task)
    end

    it "assigns a public task to @task" do
      task = FactoryBot.create(:task, private_task: false)
      get :show,  params: { id: task.id }
      expect(assigns(:task)).to eq(task)
    end
  end
  describe "POST create" do
    let(:task) { FactoryBot.create(:task) }
    context 'create task with valid attributes' do
      it 'creates a new task' do
        expect{
            post :create, params: { task: FactoryBot.attributes_for(:task) }
          }.to change(Task, :count).by(1)
      end

      it "redirects to the new task" do
        post :create, params: { task: FactoryBot.attributes_for(:task) }
        expect(response).to redirect_to Task.last
      end
    end

    context "with invalid attributes" do
      it "does not save the new task" do
        expect{
          post :create, params: { task: FactoryBot.attributes_for(:task, title: '') }
        }.to_not change(Task, :count)
      end
      it "re-renders the new method" do
        post :create, params: { task: FactoryBot.attributes_for(:task, title: '') }
        expect(response).to render_template :new
      end
    end
  end

  describe 'PUT update' do
    before :each do
      @task = FactoryBot.create(:task, user: User.first)
    end

    context "valid attributes" do

      it "located the requested @task" do
        put :update, params: { id: @task.id, task: FactoryBot.attributes_for(:task) }
        expect(assigns(:task)).to eq(@task)
      end

      it "changes @task's attributes" do
        put :update, params: { id: @task.id, task: FactoryBot.attributes_for(:task, title: 'changed') }
        @task.reload
        expect(@task.title).to eq('changed')
      end

      it "redirects to the updated task" do
        put :update, params: { id: @task.id, task: FactoryBot.attributes_for(:task) }
        expect(response).to redirect_to @task
      end
    end

    context "invalid attributes" do
      it "locates the requested @task" do
        put :update, params: { id: @task.id, task: FactoryBot.attributes_for(:task, title: '') }
        expect(assigns(:task)).to eq(@task)
      end

      it "does not change @task's attributes" do
        put :update, params: { id: @task.id, task: FactoryBot.attributes_for(:task, title: '') }
        @task.reload
        expect(@task.title).to_not eq('')
      end

      it "re-renders the edit method" do
        put :update, params: { id: @task.id, task: FactoryBot.attributes_for(:task, title: '') }
        expect(response).to render_template :edit
      end
    end
  end

  describe 'DELETE destroy' do
    before :each do
      @task = FactoryBot.create(:task, user_id: User.first.id)
    end

    it "deletes the task" do
      expect{
        delete :destroy, params: { id: @task.id }
      }.to change(Task,:count).by(-1)
    end

    it "redirects to tasks#index" do
      delete :destroy, params: { id: @task.id }
      expect(response).to redirect_to tasks_url
    end
  end
end
