require 'rails_helper'

feature 'Update Task' do
  scenario 'with valid parameters' do
    sign_in
    task = FactoryBot.create(:task, user_id: User.first.id)
    visit edit_task_path task.id
    fill_in 'task_title', with: 'title updated'
    click_button 'Update Task'
    expect(page).to have_content('Task was successfully updated.')
  end

  scenario 'with invalid parameters' do
    sign_in
    task = FactoryBot.create(:task, user_id: User.first.id)
    visit edit_task_path task.id
    fill_in 'task_title', with: ''
    click_button 'Update Task'
    expect(page).to have_content("can't be blank")
  end
end
