require 'rails_helper'

feature 'List Tasks' do
  scenario 'list my tasks' do
    sign_in
    visit tasks_path
    expect(page).to have_content('My Tasks')
  end

  scenario 'list public tasks' do
    sign_in
    visit tasks_path
    expect(page).to have_content('Another users public tasks')
  end
end
