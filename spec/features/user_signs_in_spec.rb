require 'rails_helper'

feature 'Visitor signs in' do
  scenario 'with valid email and password' do
    user = FactoryBot.create(:user)
    sign_in_with user.email, user.password

    expect(page).to have_content('Signed in successfully.')
  end

  scenario 'with invalid email' do
    sign_in_with 'invalid_email', 'password'

    expect(page).to have_content('Invalid Email or password.')
  end

  scenario 'with blank password' do
    sign_in_with 'valid@example.com', ''

    expect(page).to have_content('Invalid Email or password.')
  end

  def sign_in_with(email, password)
    visit new_user_session_path
    fill_in 'user_email', with: email
    fill_in 'user_password', with: password
    click_button 'Log in'
  end
end
