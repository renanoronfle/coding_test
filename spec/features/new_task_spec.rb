require 'rails_helper'

feature 'New Task' do
  scenario 'with valid parameters' do
    sign_in
    visit new_task_path
    fill_in 'task_title', with: 'title'
    fill_in 'task_description', with: 'description'
    click_button 'Create Task'
    expect(page).to have_content('Task was successfully created.')
  end

  scenario 'with invalid parameters' do
    sign_in
    visit new_task_path
    fill_in 'task_title', with: ''
    fill_in 'task_description', with: 'description'
    click_button 'Create Task'
    expect(page).to have_content("can't be blank")
  end
end
