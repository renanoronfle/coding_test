require 'rails_helper'

feature 'Show task' do
  scenario 'show my public task' do
    sign_in
    task = FactoryBot.create(:task, private_task: false, user_id: User.first.id)
    visit task_path task.id
    expect(page).to have_content('Details')
  end

  scenario 'show my private task' do
    sign_in
    task = FactoryBot.create(:task, user_id: User.first.id)
    visit task_path task.id
    expect(page).to have_content('Details')
  end

  scenario 'show public task another user' do
    task = FactoryBot.create(:task, private_task: false)
    sign_in
    visit task_path task.id
    expect(page).to have_content('Details')
  end
end
