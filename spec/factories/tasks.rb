FactoryBot.define do
  factory :task do
    title 'Test title'
    description 'Test description'
    private_task true
    association :user, factory: :user
  end
end
