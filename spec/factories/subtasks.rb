FactoryBot.define do
  factory :subtask do
    title "MyString"
    description "MyText"
    finished false
    finished_at "2018-03-14 02:43:28"
  end
end
