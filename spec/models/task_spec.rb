require 'rails_helper'

RSpec.describe Task, type: :model do
  context '#scopes' do
    context '#public_tasks' do
      it 'return one public task data' do
        FactoryBot.create(:task, private_task: false)
        expect(Task.public_tasks.count).to be(1)
      end

      it 'return zero public task data' do
        FactoryBot.create(:task, private_task: true)
        expect(Task.public_tasks.count).to be(0)
      end
    end

    context '#private_tasks' do
      it 'return one prive task data' do
        FactoryBot.create(:task, private_task: true)
        expect(Task.private_tasks.count).to be(1)
      end

      it 'return zero private task data' do
        FactoryBot.create(:task, private_task: false)
        expect(Task.private_tasks.count).to be(0)
      end
    end
  end
end
