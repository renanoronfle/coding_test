class Subtask < ApplicationRecord
  validates :title, :description, presence: true
  belongs_to :task
end
