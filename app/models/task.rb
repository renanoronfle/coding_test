class Task < ApplicationRecord
  validates :title, :description, presence: true
  belongs_to :user
  has_many :subtasks

  accepts_nested_attributes_for :subtasks

  scope :public_tasks, -> { where(private_task: false ) }
  scope :private_tasks, -> { where(private_task: true ) }

end
