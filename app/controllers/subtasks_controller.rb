class SubtasksController < BaseController

  def create
    @subtask = Subtask.new subtask_params
    @subtask.task_id = params[:task_id]
    @subtask.save
    respond_with @subtask.task, notice: 'SubTask was successfully created.'
  end

  def destroy
    task = current_user.tasks.find params[:task_id]
    subtask =  task.subtasks.find(params[:id])
    subtask.destroy
    redirect_to task_path(task.id)
  end

  def update
    @task = current_user.tasks.find params[:id]
    @task.update(task_params)
    respond_with @task
  end

  private

  def subtask_params
    params.require(:subtask).permit(:title, :description)
  end
end
