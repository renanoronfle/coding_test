class TasksController < BaseController

  def index
    @my_tasks = current_user.tasks
    @public_tasks = Task.where(private_task: false).where.not(user_id: current_user.id)
  end

  def new
    @task = Task.new
    respond_with @task
  end

  def create
    @task = Task.new task_params
    @task.user = current_user
    @task.save
    respond_with @task
  end

  def show
    task = Task.find params[:id]
    @subtask = task.subtasks.build
    @subtasks = task.subtasks
    @task = task if (task.user == current_user || !task.private_task)
    respond_with @task
  end

  def edit
    @task = current_user.tasks.find params[:id]
    respond_with @task
  end

  def destroy
    @task = current_user.tasks.find params[:id]
    @task.destroy
    respond_with @task
  end

  def update
    @task = current_user.tasks.find params[:id]
    @task.update(task_params)
    respond_with @task
  end

  private

  def task_params
    params.require(:task).permit(:title, :private, :description, :finished)
  end
end
