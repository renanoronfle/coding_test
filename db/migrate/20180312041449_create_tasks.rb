class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.references :user, foreign_key: true
      t.boolean :private_task, default: false
      t.string :title
      t.text :description
      t.boolean :finished
      t.datetime :finished_at

      t.timestamps
    end
  end
end
