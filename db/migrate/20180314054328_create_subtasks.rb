class CreateSubtasks < ActiveRecord::Migration[5.1]
  def change
    create_table :subtasks do |t|
      t.references :task, foreign_key: true
      t.string :title
      t.text :description
      t.boolean :finished
      t.datetime :finished_at

      t.timestamps
    end
  end
end
